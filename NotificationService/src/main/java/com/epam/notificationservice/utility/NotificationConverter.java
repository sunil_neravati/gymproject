package com.epam.notificationservice.utility;

import java.util.Map;
import java.util.stream.Collectors;

import com.epam.notificationservice.dto.Constants;
import com.epam.notificationservice.dto.NotificationDTO;
import com.epam.notificationservice.entity.Notification;

public class NotificationConverter {

	private NotificationConverter() {
	}

	public static Notification getNotification(NotificationDTO notificationDTO, String status, String remarks) {
		Notification notification = new Notification();
		notification.setBody(getBody(notificationDTO.getParameters()));
		notification.setToEmails(notificationDTO.getToEmails());
		notification.setCcEmails(notificationDTO.getCcEmails());
		notification.setRemarks(remarks);
		notification.setStatus(status);
		notification.setSubject(Constants.valueOf(notificationDTO.getEmailType()).getMessage());
		return notification;
	}

	public static String getBody(Map<String, String> parameters) {

		return parameters.entrySet().stream().map(entry -> entry.getKey() + ": " + entry.getValue())
				.collect(Collectors.joining("\n"));
	}

}
