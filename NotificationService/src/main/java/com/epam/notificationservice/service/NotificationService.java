package com.epam.notificationservice.service;

import com.epam.notificationservice.dto.NotificationDTO;
import com.epam.notificationservice.dto.NotificationResponse;

public interface NotificationService {
	
	NotificationResponse sendNotification(NotificationDTO notificationDTO);

}
