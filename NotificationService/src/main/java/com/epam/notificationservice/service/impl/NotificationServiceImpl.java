package com.epam.notificationservice.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.epam.notificationservice.dto.Constants;
import com.epam.notificationservice.dto.NotificationDTO;
import com.epam.notificationservice.dto.NotificationResponse;
import com.epam.notificationservice.dto.Status;
import com.epam.notificationservice.entity.Notification;
import com.epam.notificationservice.entity.NotificationRepository;
import com.epam.notificationservice.service.NotificationService;
import com.epam.notificationservice.utility.NotificationConverter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationServiceImpl implements NotificationService {

	private final JavaMailSender javaMailSender;
	private final NotificationRepository notificationRepository;

	@Value("${fromMail}")
	private String fromMail;

	@Override
	public NotificationResponse sendNotification(NotificationDTO notificationDTO) {

		log.info("Entered sendNotification method to send the mail");

		String status;
		String remarks;
		SimpleMailMessage message = new SimpleMailMessage();

		if (notificationDTO.getToEmails() != null && !notificationDTO.getToEmails().isEmpty()) {
			String[] toArray = notificationDTO.getToEmails().toArray(new String[notificationDTO.getToEmails().size()]);
			message.setTo(toArray);
		}

		if (notificationDTO.getCcEmails() != null && !notificationDTO.getCcEmails().isEmpty()) {
			String[] ccArray = notificationDTO.getCcEmails().toArray(new String[notificationDTO.getCcEmails().size()]);
			message.setCc(ccArray);
		}

		message.setSubject(Constants.valueOf(notificationDTO.getEmailType()).getMessage());
		message.setText(NotificationConverter.getBody(notificationDTO.getParameters()));
		message.setFrom(fromMail);

		try {
			javaMailSender.send(message);
			status = Status.SENT.toString();
			remarks = "Mail sent Successfully";
		} catch (Exception e) {
			status = Status.FAILED.toString();
			remarks = e.getMessage();
		}

		Notification notification = NotificationConverter.getNotification(notificationDTO, status, remarks);
		notification.setFromEmail(fromMail);
		notificationRepository.save(notification);

		return NotificationResponse.builder().status(status).remarks(remarks).build();
	}

}
