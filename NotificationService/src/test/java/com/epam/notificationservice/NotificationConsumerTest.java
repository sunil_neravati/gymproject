package com.epam.notificationservice;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.notificationservice.dto.NotificationDTO;
import com.epam.notificationservice.listener.Consumer;
import com.epam.notificationservice.service.NotificationService;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
class NotificationConsumerTest {

    @Mock
    private NotificationService notificationService;

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private Consumer notificationConsumer;

    @Captor
    private ArgumentCaptor<NotificationDTO> notificationCaptor;

    @Test
    void testSendNotification() throws Exception {
        String jsonMessage = "{\"key\": \"value\"}"; 
        NotificationDTO expectedNotification = new NotificationDTO();
        when(objectMapper.readValue(jsonMessage, NotificationDTO.class)).thenReturn(expectedNotification);

        notificationConsumer.sendNotification(jsonMessage);

        verify(notificationService, times(1)).sendNotification(notificationCaptor.capture());
        NotificationDTO capturedNotification = notificationCaptor.getValue();
        assertEquals(expectedNotification, capturedNotification);
    }

}