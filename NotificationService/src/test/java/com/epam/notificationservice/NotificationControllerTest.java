package com.epam.notificationservice;


import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.notificationservice.controller.NotificationController;
import com.epam.notificationservice.dto.NotificationDTO;
import com.epam.notificationservice.dto.NotificationResponse;
import com.epam.notificationservice.dto.Status;
import com.epam.notificationservice.entity.NotificationRepository;
import com.epam.notificationservice.service.NotificationService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(NotificationController.class)
class NotificationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NotificationService notificationService;

    @MockBean
    private NotificationRepository notificationRepository;

    private NotificationDTO notificationDTO;

    private NotificationResponse notificationResponse;

    @BeforeEach
    void setNotificationDTO() {
        notificationDTO = new NotificationDTO();
        notificationDTO.setToEmails(Collections.singletonList("to@example.com"));
        notificationDTO.setCcEmails(Collections.singletonList("cc@example.com"));
        notificationDTO.setEmailType("REGISTRATION");
        notificationDTO.setParameters(Collections.singletonMap("key", "value"));
    }

    @BeforeEach
    void setNotificationResponse() {
        notificationResponse = new NotificationResponse();
        notificationResponse.setRemarks("Mail sent Successfully");
        notificationResponse.setStatus(Status.SENT.toString());
    }

    @Test
    void testSendMail() throws Exception {
        Mockito.when(notificationService.sendNotification(notificationDTO)).thenReturn(notificationResponse);

        mockMvc.perform(post("/notification/send").contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(notificationDTO))).andExpect(status().isOk());

        Mockito.verify(notificationService).sendNotification(any(NotificationDTO.class));
    }

}
