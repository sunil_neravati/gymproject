package com.epam.identityservice;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.identityservice.service.AuthService;
import com.epam.identityservice.service.JwtService;

@ExtendWith(MockitoExtension.class)
class AuthServiceTest {

    @Mock
    private JwtService jwtService;

    @InjectMocks
    private AuthService authService;
    
    @Test
    void testGenerateToken() {
        String username = "testUser";
        when(jwtService.generateToken(username)).thenReturn("mockedToken");
        String token = authService.generateToken(username);
        verify(jwtService).generateToken(username);
        assertEquals("mockedToken", token);
    }
    
    @Test
    void testValidateToken() {
        String token = "mockedToken";
        authService.validateToken(token);
        verify(jwtService).validateToken(token);
    }

}