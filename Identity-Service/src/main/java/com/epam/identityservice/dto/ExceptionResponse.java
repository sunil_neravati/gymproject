package com.epam.identityservice.dto; 

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class ExceptionResponse {

	String timeStamp;
	String status;
	String error;
	String path;

}
