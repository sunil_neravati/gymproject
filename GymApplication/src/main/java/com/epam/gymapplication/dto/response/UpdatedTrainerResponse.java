package com.epam.gymapplication.dto.response;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class UpdatedTrainerResponse {

	
	private String firstName;

	private String lastName;
	
	private String username;
	
	private String specialization;
	
	private String email;
	
	private Boolean isActive;
	
	private List<TraineeResponse> trainees;
}
