package com.epam.gymapplication.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class AuthRequest {
	
	@NotBlank(message = "username should not be blank")
	private String username;

	@NotBlank(message = "password should not be blank")
	private String password;

}
