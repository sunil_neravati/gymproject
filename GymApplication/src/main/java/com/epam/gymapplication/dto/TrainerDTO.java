package com.epam.gymapplication.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TrainerDTO {

	@NotBlank(message = "FirstName should not be blank")
	private String firstName;
	
	@NotBlank(message = "LastName should not be blank")
	private String lastName;
	
	@Email
	private String email;

	@Pattern(regexp = "^(?i)(fitness|yoga|Zumba|stretching|resistance)$", message = "Invalid fitness type")
	private String specialization;

}
