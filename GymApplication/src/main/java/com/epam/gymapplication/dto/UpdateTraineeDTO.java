package com.epam.gymapplication.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class UpdateTraineeDTO {

	@NotBlank(message = "FirstName should not be blank")
	private String firstName;

	@NotBlank(message = "LastName should not be blank")
	private String lastName;

	@NotNull(message = "date of birth should not be empty")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfBirth;

	@NotBlank(message = "address should not be empty")
	private String address;

	@Email(message = "email should be valid")
	private String email;

	@NotBlank(message = "username should not be empty")
	private String username;

	@NotNull(message = "Please set the active status")
	private Boolean isActive;

}
