package com.epam.gymapplication.producer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.epam.gymapplication.dto.TrainingReportDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.annotation.PreDestroy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReportProducer {

	private final KafkaTemplate<String, String> reportTemplate;

	private final ObjectMapper objectMapper;

	@Value("${report-topic}")
	private String reportTopic;

	public void sendReport(TrainingReportDTO trainingReportDTO) throws JsonProcessingException {

		log.info("Entered send Report method, TrainingReportDTO : {}", trainingReportDTO);
		String message = objectMapper.writeValueAsString(trainingReportDTO);
		try {
			reportTemplate.send(reportTopic, message);
		} catch (Exception e) {
			log.error(String.format("Something went wrong : %s", e.getMessage()));
		}
	}

	@PreDestroy
	public void close() {
		if (reportTemplate != null) {
			log.info("Closing Kafka Producer");
			reportTemplate.destroy();
		}
	}

}
