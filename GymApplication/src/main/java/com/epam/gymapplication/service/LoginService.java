package com.epam.gymapplication.service;

public interface LoginService {
	
	public void login(String userName,String password);
	
	public void changePassword(String userName,String oldPassword,String newPassword);

}
