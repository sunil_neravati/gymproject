package com.epam.gymapplication.service.impl;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gymapplication.dao.UserRepository;
import com.epam.gymapplication.entity.User;
import com.epam.gymapplication.exception.UserException;
import com.epam.gymapplication.service.LoginService;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class LoginServiceImpl implements LoginService {

	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;


    @Override
    public void login(String userName, String password) {
        
        log.info("Entered login method, username : {},password : {}",userName,password);
        
        User user = userRepository.findByUsername(userName).orElseThrow(() -> new UserException("User not Found"));
        
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new UserException("Password is not correct");
        }
        
    }

    @Override
    @Transactional
    public void changePassword(String userName, String oldPassword, String newpassWord) {
        
        log.info("Entered change password method, username : {}, oldpassword : {}, newpassword : {}",userName,oldPassword,newpassWord);
        User user=userRepository.findByUsername(userName)
                .orElseThrow(() -> new UserException("Invalid user details"));
        
        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw new UserException("Password is not correct");
        }else {
            user.setPassword(passwordEncoder.encode(newpassWord));
        }
        
    }
}