package com.epam.gymapplication.service;

import com.epam.gymapplication.dto.TrainerDTO;
import com.epam.gymapplication.dto.UpdateTrainerDTO;
import com.epam.gymapplication.dto.response.RegistrationResponse;
import com.epam.gymapplication.dto.response.TrainerProfileResponse;
import com.epam.gymapplication.dto.response.UpdatedTrainerResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface TrainerService {
	
	public RegistrationResponse trainerRegistration(TrainerDTO trainerDTO) throws JsonProcessingException;
	
	public TrainerProfileResponse getTrainerProfileDetails(String username);
	
	public UpdatedTrainerResponse updateTrainerDetails(UpdateTrainerDTO updateTrainerDTO)throws JsonProcessingException;
	
		
}
