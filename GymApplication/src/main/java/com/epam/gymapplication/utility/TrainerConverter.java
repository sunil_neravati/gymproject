package com.epam.gymapplication.utility;

import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.gymapplication.dto.response.TraineeResponse;
import com.epam.gymapplication.dto.response.TrainerProfileResponse;
import com.epam.gymapplication.dto.response.TrainerResponse;
import com.epam.gymapplication.dto.response.UpdatedTrainerResponse;
import com.epam.gymapplication.entity.Trainer;
import com.epam.gymapplication.entity.User;

@Component
public class TrainerConverter {

	public TrainerResponse getTrainerResponse(Trainer trainer, User tempUser) {
		TrainerResponse trainerResponse = new TrainerResponse();
		trainerResponse.setSpecialization(trainer.getSpecialization().getTrainingTypeName());
		trainerResponse.setFirstName(tempUser.getFirstName());
		trainerResponse.setLastName(tempUser.getLastName());
		trainerResponse.setUsername(tempUser.getUsername());
		return trainerResponse;
	}


	public  TrainerProfileResponse getTrainerProfileResponse(Trainer trainer, User user,
			List<TraineeResponse> traineeProfiles) {
		TrainerProfileResponse trainerProfileResponse = new TrainerProfileResponse();
		trainerProfileResponse.setFirstName(user.getFirstName());
		trainerProfileResponse.setLastName(user.getLastName());
		trainerProfileResponse.setSpecialization(trainer.getSpecialization().getTrainingTypeName());
		trainerProfileResponse.setIsActive(user.isActive());
		trainerProfileResponse.setTrainees(traineeProfiles);
		return trainerProfileResponse;
	}
	
	public  UpdatedTrainerResponse getUpdatedResponse(Trainer trainer, User user,
			List<TraineeResponse> traineeProfiles) {
		UpdatedTrainerResponse trainerProfileResponse = new UpdatedTrainerResponse();
		trainerProfileResponse.setFirstName(user.getFirstName());
		trainerProfileResponse.setLastName(user.getLastName());
		trainerProfileResponse.setUsername(user.getUsername());
		trainerProfileResponse.setEmail(user.getEmail());
		trainerProfileResponse.setSpecialization(trainer.getSpecialization().getTrainingTypeName());
		trainerProfileResponse.setIsActive(user.isActive());
		trainerProfileResponse.setTrainees(traineeProfiles);
		return trainerProfileResponse;
	}

}
