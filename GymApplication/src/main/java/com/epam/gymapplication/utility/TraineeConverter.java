package com.epam.gymapplication.utility;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.gymapplication.dto.TraineeDTO;
import com.epam.gymapplication.dto.response.TraineeProfileResponse;
import com.epam.gymapplication.dto.response.TraineeResponse;
import com.epam.gymapplication.dto.response.TrainerResponse;
import com.epam.gymapplication.dto.response.UpdatedTraineeResponse;
import com.epam.gymapplication.entity.Trainee;
import com.epam.gymapplication.entity.User;

@Component
public class TraineeConverter {
	
	public User getUserFromTraineeDTO(TraineeDTO traineeDTO, String username,String password) {
		return User.builder().firstName(traineeDTO.getFirstName()).lastName(traineeDTO.getLastName()).username(username)
				.password(password).email(traineeDTO.getEmail()).createdDate(new Date())
				.isActive(true).build();
	}

	public  TraineeProfileResponse getTraineeProfileResponse(Trainee trainee, User user,
			List<TrainerResponse> trainerProfiles) {
		TraineeProfileResponse traineeProfileResponse = new TraineeProfileResponse();
		traineeProfileResponse.setFirstName(user.getFirstName());
		traineeProfileResponse.setLastName(user.getLastName());
		traineeProfileResponse.setDateOfBirth(trainee.getDateOfBirth());
		traineeProfileResponse.setAddress(trainee.getAddress());
		traineeProfileResponse.setActive(user.isActive());
		traineeProfileResponse.setTrainers(trainerProfiles);
		return traineeProfileResponse;
	}
	public  UpdatedTraineeResponse getUpdatedTraineeResponse(Trainee trainee, User user,
			List<TrainerResponse> trainerProfiles) {
		UpdatedTraineeResponse traineeProfileResponse = new UpdatedTraineeResponse();
		traineeProfileResponse.setFirstName(user.getFirstName());
		traineeProfileResponse.setLastName(user.getLastName());
		traineeProfileResponse.setUsername(user.getUsername());
		traineeProfileResponse.setEmail(user.getEmail());
		traineeProfileResponse.setDateOfBirth(trainee.getDateOfBirth());
		traineeProfileResponse.setAddress(trainee.getAddress());
		traineeProfileResponse.setActive(user.isActive());
		traineeProfileResponse.setTrainers(trainerProfiles);
		return traineeProfileResponse;
	}
	public TraineeResponse getTraineeResponse(User tempUser) {
		TraineeResponse traineeResponse = new TraineeResponse();
		traineeResponse.setFirstName(tempUser.getFirstName());
		traineeResponse.setLastName(tempUser.getLastName());
		traineeResponse.setUsername(tempUser.getUsername());
		return traineeResponse;
	}
}
