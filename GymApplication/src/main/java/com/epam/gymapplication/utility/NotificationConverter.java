package com.epam.gymapplication.utility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.epam.gymapplication.dto.NotificationDTO;
import com.epam.gymapplication.dto.TrainingDTO;
import com.epam.gymapplication.dto.TrainingReportDTO;
import com.epam.gymapplication.dto.response.UpdatedTraineeResponse;
import com.epam.gymapplication.dto.response.UpdatedTrainerResponse;
import com.epam.gymapplication.entity.Trainee;
import com.epam.gymapplication.entity.Trainer;
import com.epam.gymapplication.entity.User;

@Component
public class NotificationConverter {

	public TrainingReportDTO getTrainingReportDTO(TrainingDTO trainingDTO, Trainer trainer) {
		TrainingReportDTO trainingReportDTO = new TrainingReportDTO();
		User user = trainer.getUser();
		trainingReportDTO.setFirstName(user.getFirstName());
		trainingReportDTO.setLastName(user.getLastName());
		trainingReportDTO.setUsername(user.getUsername());
		trainingReportDTO.setIsActive(user.isActive());
		trainingReportDTO.setTrainingDate(trainingDTO.getTrainingDate());
		trainingReportDTO.setTrainingDuration(trainingDTO.getDuration());
		trainingReportDTO.setTrainingName(trainingDTO.getTrainingName());
		return trainingReportDTO;
	}

	public NotificationDTO getNotificationDTO(User user) {
		NotificationDTO notificationDTO = new NotificationDTO();

		Map<String, String> map = new HashMap<>();
		map.put("Username", user.getUsername());
		map.put("Password", user.getPassword());
		notificationDTO.setEmailType("REGISTRATION");
		notificationDTO.setParameters(map);
		notificationDTO.setToEmails(List.of(user.getEmail()));
		return notificationDTO;
	}

	public NotificationDTO getNotificationDTO(UpdatedTraineeResponse updatedTraineeResponse) {
		NotificationDTO notificationDTO = new NotificationDTO();

		Map<String, String> map = new HashMap<>();
		map.put("Username", updatedTraineeResponse.getUsername());
		map.put("Email", updatedTraineeResponse.getEmail());
		map.put("Address", updatedTraineeResponse.getAddress());
		map.put("Firstname", updatedTraineeResponse.getFirstName());
		map.put("Lastname", updatedTraineeResponse.getLastName());
		map.put("Dateofbirth", updatedTraineeResponse.getDateOfBirth().toString());
		notificationDTO.setParameters(map);
		notificationDTO.setEmailType("UPDATE_TRAINEE");
		notificationDTO.setToEmails(List.of(updatedTraineeResponse.getEmail()));
		return notificationDTO;
	}

	public NotificationDTO getNotificationDTO(UpdatedTrainerResponse updatedTrainerResponse) {
		NotificationDTO notificationDTO = new NotificationDTO();
		Map<String, String> map = new HashMap<>();
		map.put("Username", updatedTrainerResponse.getUsername());
		map.put("Email", updatedTrainerResponse.getEmail());
		map.put("Firstname", updatedTrainerResponse.getFirstName());
		map.put("Lastname", updatedTrainerResponse.getLastName());
		map.put("Specialisation", updatedTrainerResponse.getSpecialization());
		notificationDTO.setParameters(map);
		notificationDTO.setEmailType("UPDATE_TRAINER");
		notificationDTO.setToEmails(List.of(updatedTrainerResponse.getEmail()));
		return notificationDTO;
	}

	public NotificationDTO getNotificationDTO(Trainee trainee, Trainer trainer, TrainingDTO trainingDTO) {

		NotificationDTO notificationDTO = new NotificationDTO();
		Map<String, String> map = new HashMap<>();
		map.put("Traineename", trainee.getUser().getUsername());
		map.put("Trainername", trainer.getUser().getUsername());
		map.put("Date", trainingDTO.getTrainingDate().toString());
		map.put("Duration", trainingDTO.getDuration() + "");
		map.put("Training type", trainingDTO.getTrainingType());
		notificationDTO.setParameters(map);
		notificationDTO.setEmailType("TRAINING_REGISTRATION");
		notificationDTO.setToEmails(List.of(trainee.getUser().getEmail(), trainer.getUser().getEmail()));
		return notificationDTO;
	}

}
