package com.epam.gymapplication.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.gymapplication.entity.Trainee;

@Repository
public interface TraineeRepository extends JpaRepository<Trainee, Integer> {

	Optional<Trainee> findByUserUsername(String username);

}
