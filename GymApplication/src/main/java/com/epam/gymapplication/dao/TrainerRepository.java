package com.epam.gymapplication.dao;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.gymapplication.entity.Trainee;
import com.epam.gymapplication.entity.Trainer;

public interface TrainerRepository extends JpaRepository<Trainer, Integer> {

	Optional<Trainer> findByUserUsername(String username);

	Set<Trainer> findByUserUsernameIn(List<String> trainerNames);

	List<Trainer> findByTraineesNotContaining(Trainee trainee);

}
