package com.epam.gymapplication.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapplication.service.LoginService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/gym")
public class LoginController {

	private final LoginService loginService;

	@GetMapping("/updatepassword")
	public ResponseEntity<Void> updatePassword(@RequestParam String userName, @RequestParam String oldPassword,
			@RequestParam String newPassword) {

		loginService.changePassword(userName, oldPassword, newPassword);
		return new ResponseEntity<>(HttpStatus.OK);

	}
}
