package com.epam.gymapplication.exceptionalhandler;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.gymapplication.dto.response.ExceptionResponse;
import com.epam.gymapplication.exception.TraineeException;
import com.epam.gymapplication.exception.TrainerException;
import com.epam.gymapplication.exception.TrainingException;
import com.epam.gymapplication.exception.UserException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ExceptionResponse> argumentValidationException(MethodArgumentNotValidException errors,
			WebRequest request) {
		Map<String, String> result = new HashMap<>();
		errors.getBindingResult().getFieldErrors().forEach(error -> result.put(error.getField(), error.getDefaultMessage()));
		String message = result.toString();
		log.error(message);
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), message,
				request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ExceptionResponse> runtimeExceptionHandler(RuntimeException message, WebRequest request) {
		log.error("Something went wrong " + message.getMessage());
		return new ResponseEntity<>(
				new ExceptionResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.name(),
						"Something went wrong " + message.getMessage(), request.getDescription(false)),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ExceptionResponse> dataIntegrityExceptionalHandler(DataIntegrityViolationException message,
			WebRequest request) {
		log.error(message.getMessage());
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				message.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserException.class)
	public ResponseEntity<ExceptionResponse> userNotFoundExceptionalHandler(UserException message, WebRequest request) {
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				message.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(TraineeException.class)
	public ResponseEntity<ExceptionResponse> traineeNotFoundExceptionalHandler(TraineeException message,
			WebRequest request) {
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				message.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(TrainerException.class)
	public ResponseEntity<ExceptionResponse> trainerNotFoundExceptionalHandler(TrainerException message,
			WebRequest request) {
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				message.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(TrainingException.class)
	public ResponseEntity<ExceptionResponse> trainingNotFoundExceptionalHandler(TrainingException message,
			WebRequest request) {
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				message.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}
}
