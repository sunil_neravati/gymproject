package com.epam.gymapplication;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.gymapplication.dao.UserRepository;
import com.epam.gymapplication.entity.User;
import com.epam.gymapplication.exception.UserException;
import com.epam.gymapplication.service.impl.LoginServiceImpl;

@ExtendWith(MockitoExtension.class)
class LoginServiceTests {

	@Mock
	private UserRepository userRepository;

	@Mock
	private PasswordEncoder passwordEncoder;

	@InjectMocks
	private LoginServiceImpl loginService;

	String username;
	String password;
	String encodedPassword;
	String encodedOldPassword;
	String encodedNewPassword;
	String oldPassword;
	String newPassword;
	User user;

	@BeforeEach
	void setUp() {

		username = "testUser";
		password = "testPassword";
		encodedPassword = "encodedPassword";

		oldPassword = "oldPassword";
		newPassword = "newPassword";
		encodedOldPassword = "encodedOldPassword";
		encodedNewPassword = "encodedNewPassword";

		user = new User();
		user.setUsername(username);
		user.setPassword(encodedPassword);
	}

	@Test
	void testLoginSuccessful() {

		Mockito.when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
		Mockito.when(passwordEncoder.matches(password, encodedPassword)).thenReturn(true);

		loginService.login(username, password);

		verify(userRepository, times(1)).findByUsername(username);
		verify(passwordEncoder, times(1)).matches(password, encodedPassword);
	}

	@Test
	void testLoginInvalidPassword() {
	

		when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
		when(passwordEncoder.matches(password, encodedPassword)).thenReturn(false);

		assertThrows(UserException.class, () -> loginService.login(username, password));

		verify(userRepository, times(1)).findByUsername(username);
		verify(passwordEncoder, times(1)).matches(password, encodedPassword);
	}

	@Test
	void testLoginUserNotFound() {

		when(userRepository.findByUsername(username)).thenReturn(java.util.Optional.empty());

		assertThrows(UserException.class, () -> loginService.login(username, password));

		verify(userRepository, times(1)).findByUsername(username);
		verify(passwordEncoder, never()).matches(anyString(), anyString());
	}

	@Test
	void testChangePasswordSuccessful() {

		user.setPassword(encodedOldPassword);

		when(userRepository.findByUsername(username)).thenReturn(java.util.Optional.of(user));
		when(passwordEncoder.matches(oldPassword, encodedOldPassword)).thenReturn(true);
		when(passwordEncoder.encode(newPassword)).thenReturn(encodedNewPassword);

		loginService.changePassword(username, oldPassword, newPassword);

		verify(userRepository, times(1)).findByUsername(username);
		verify(passwordEncoder, times(1)).matches(oldPassword, encodedOldPassword);
		verify(passwordEncoder, times(1)).encode(newPassword);
	}

	@Test
	void testChangePasswordInvalidOldPassword() {

		user.setPassword(encodedOldPassword);

		when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
		when(passwordEncoder.matches(oldPassword, encodedOldPassword)).thenReturn(false);

		assertThrows(UserException.class, () -> loginService.changePassword(username, oldPassword, newPassword));

		verify(userRepository, times(1)).findByUsername(username);
		verify(passwordEncoder, times(1)).matches(oldPassword, encodedOldPassword);
		verify(passwordEncoder, never()).encode(anyString());
	}

	@Test
	void testChangePasswordUserNotFound() {
		when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

		assertThrows(UserException.class, () -> loginService.changePassword(username, oldPassword, newPassword));

		verify(userRepository, times(1)).findByUsername(username);
		verify(passwordEncoder, never()).matches(anyString(), anyString());
		verify(passwordEncoder, never()).encode(anyString());
	}
}
