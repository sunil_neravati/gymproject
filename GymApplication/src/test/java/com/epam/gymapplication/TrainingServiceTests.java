package com.epam.gymapplication;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.gymapplication.dao.TraineeRepository;
import com.epam.gymapplication.dao.TrainerRepository;
import com.epam.gymapplication.dao.TrainingRepository;
import com.epam.gymapplication.dao.UserRepository;
import com.epam.gymapplication.dto.NotificationDTO;
import com.epam.gymapplication.dto.TrainingDTO;
import com.epam.gymapplication.dto.TrainingReportDTO;
import com.epam.gymapplication.dto.response.TrainingResponse;
import com.epam.gymapplication.entity.Trainee;
import com.epam.gymapplication.entity.Trainer;
import com.epam.gymapplication.entity.Training;
import com.epam.gymapplication.entity.TrainingType;
import com.epam.gymapplication.entity.User;
import com.epam.gymapplication.exception.TraineeException;
import com.epam.gymapplication.exception.TrainerException;
import com.epam.gymapplication.exception.TrainingException;
import com.epam.gymapplication.exception.UserException;
import com.epam.gymapplication.producer.NotificationProducer;
import com.epam.gymapplication.producer.ReportProducer;
import com.epam.gymapplication.service.impl.TrainingServiceImpl;
import com.epam.gymapplication.utility.TrainingConverter;
import com.epam.gymapplication.utility.NotificationConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

@ExtendWith(MockitoExtension.class)
class TrainingServiceTests {

	@Mock
	private TrainingRepository trainingRepository;

	@Mock
	private TrainerRepository trainerRepository;

	@Mock
	private TraineeRepository traineeRepository;

	@Mock
	private UserRepository userRepository;

	@Mock
	private NotificationProducer notificationProducer;

	@Mock
	private ReportProducer reportProducer;

	@Mock
	private TrainingConverter converter;
	
	@Mock
	private NotificationConverter notificationConverter;

	@InjectMocks
	private TrainingServiceImpl trainingService;

	private TrainingDTO trainingDTO;

	private Trainee trainee;

	private Trainer trainer;

	private TrainingType trainingType;

	Set<Trainer> trainersSet;

	Set<Trainee> traineesSet;

	String username;
	Date periodFrom;
	Date periodTo;
	String traineeName;
	String trainerName;
	String trainingType1;

	User user;

	@BeforeEach
	void setup() {

		trainingDTO = new TrainingDTO();
		trainingDTO.setTraineeUsername("traineeUser");
		trainingDTO.setTrainerUsername("trainerUser");
		trainingDTO.setTrainingType("zumba");

		trainee = new Trainee();
		trainer = new Trainer();
		trainingType = new TrainingType();
		trainingType.setTrainingTypeName("zumba");
		trainer.setSpecialization(trainingType);

		trainersSet = new HashSet<>();
		trainersSet.add(trainer);
		trainee.setTrainers(trainersSet);

		traineesSet = new HashSet<>();
		traineesSet.add(trainee);
		trainer.setTrainees(traineesSet);

		username = "trainerUser";
		periodFrom = new Date();
		periodTo = new Date();
		traineeName = "traineeUser";
		trainerName = "trainerUser";

		user = new User();
		user.setUsername(username);

		trainingType1 = "type";

	}

	@Test
	void testAddTrainingSuccessful() throws JsonProcessingException {
		
		when(traineeRepository.findByUserUsername("traineeUser")).thenReturn(Optional.of(trainee));
		when(trainerRepository.findByUserUsername("trainerUser")).thenReturn(Optional.of(trainer));
		when(trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee, trainingDTO.getTrainingDate()))
				.thenReturn(Optional.empty());

		when(converter.convertTrainingDTOtoTraining(any(), any(), any())).thenReturn(new Training());
		when(converter.getTrainingReportDTO(any(), any())).thenReturn(new TrainingReportDTO());
		when(notificationConverter.getNotificationDTO(any(), any(), any())).thenReturn(new NotificationDTO());

		trainingService.addTraining(trainingDTO);

		verify(trainingRepository, times(1)).save(any());
		verify(reportProducer, times(1)).sendReport(any());
		verify(notificationProducer, times(1)).sendNotification(any(NotificationDTO.class));
	}

	@Test
	void testAddTrainingTraineeNotFound() {
		
		when(traineeRepository.findByUserUsername("traineeUser")).thenReturn(Optional.empty());

		assertThrows(TraineeException.class, () -> trainingService.addTraining(trainingDTO));
	}

	@Test
	void testAddTrainingTrainerNotFound() {
	

		when(traineeRepository.findByUserUsername("traineeUser")).thenReturn(Optional.of(new Trainee()));
		when(trainerRepository.findByUserUsername("trainerUser")).thenReturn(Optional.empty());

		assertThrows(TrainerException.class, () -> trainingService.addTraining(trainingDTO));
	}

	@Test
	void testAddTrainingTraineeAlreadyAssigned() {

		when(traineeRepository.findByUserUsername("traineeUser")).thenReturn(Optional.of(trainee));
		when(trainerRepository.findByUserUsername("trainerUser")).thenReturn(Optional.of(trainer));
		when(trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee, trainingDTO.getTrainingDate()))
				.thenReturn(Optional.of(new Training()));

		assertThrows(TrainingException.class, () -> trainingService.addTraining(trainingDTO));
	}

	@Test
	void testAddTrainingTrainerSpecializationMismatch() {
		trainingDTO.setTrainingType("invalidType");

		when(traineeRepository.findByUserUsername("traineeUser")).thenReturn(Optional.of(trainee));
		when(trainerRepository.findByUserUsername("trainerUser")).thenReturn(Optional.of(trainer));
		when(trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee, trainingDTO.getTrainingDate()))
				.thenReturn(Optional.empty());

		assertThrows(TrainerException.class, () -> trainingService.addTraining(trainingDTO));
	}

	@Test
	void testAddTrainingUnassignedTrainee() {

		Trainee trainee = new Trainee();
		User user = new User();
		user.setUsername("traineeUser");
		trainee.setUser(user);
		Trainer trainer = new Trainer();
		User user1 = new User();
		user.setUsername("trainerUser");
		trainer.setUser(user1);

		TrainingType trainingType = new TrainingType();
		trainingType.setTrainingTypeName("zumba");
		trainer.setSpecialization(trainingType);

		when(traineeRepository.findByUserUsername("traineeUser")).thenReturn(Optional.of(trainee));
		when(trainerRepository.findByUserUsername("trainerUser")).thenReturn(Optional.of(trainer));
		when(trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee, trainingDTO.getTrainingDate()))
				.thenReturn(Optional.empty());

		assertThrows(TrainerException.class, () -> trainingService.addTraining(trainingDTO));
	}

	@Test
	void testGetTraineeTrainings() {

		User user = new User();
		user.setUsername(username);

		when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
		when(trainingRepository.getTraineeTrainings(username, periodFrom, periodTo, trainerName, trainingType1))
				.thenReturn(List.of(new TrainingResponse()));

		trainingService.getTraineeTrainings(username, periodFrom, periodTo, trainerName, trainingType1);

		verify(userRepository, times(1)).findByUsername(username);
		verify(trainingRepository, times(1)).getTraineeTrainings(username, periodFrom, periodTo, trainerName,
				trainingType1);
	}

	@Test
	void testGetTraineeTrainingsWithInvalidTrainer() {

		User user = new User();
		user.setUsername(username);

		when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

		assertThrows(UserException.class,
				() -> trainingService.getTraineeTrainings(username, periodFrom, periodTo, trainerName, trainingType1));

		verify(userRepository, times(1)).findByUsername(username);

	}

	@Test
	void testGetTrainerTrainings() {

		User user = new User();
		user.setUsername(username);

		when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
		when(trainingRepository.getTrainerTrainings(username, periodFrom, periodTo, traineeName))
				.thenReturn(List.of(new TrainingResponse()));

		trainingService.getTrainerTrainings(username, periodFrom, periodTo, traineeName);

		verify(userRepository, times(1)).findByUsername(username);
		verify(trainingRepository, times(1)).getTrainerTrainings(username, periodFrom, periodTo, traineeName);
	}

	@Test
	void testGetTrainerTrainingsWithInvalidTrainer() {
		

		when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

		assertThrows(UserException.class,
				() -> trainingService.getTrainerTrainings(username, periodFrom, periodTo, traineeName));

		verify(userRepository, times(1)).findByUsername(username);
	}
}
