package com.epam.reporting.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.reporting.dto.TrainingReportDTO;
import com.epam.reporting.dto.TrainingSummaryResponse;
import com.epam.reporting.service.ReportingService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/report")
@RequiredArgsConstructor
public class ReportingController {
	
	private final ReportingService reportingService;
	
	@PostMapping
	public ResponseEntity<Void> postReport(@RequestBody @Valid TrainingReportDTO trainingReportDTO){
		reportingService.updateReport(trainingReportDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/{username}")
	public ResponseEntity<TrainingSummaryResponse> getReport(@PathVariable String username){
		return new ResponseEntity<>(reportingService.getReport(username),HttpStatus.OK);
	}

}
