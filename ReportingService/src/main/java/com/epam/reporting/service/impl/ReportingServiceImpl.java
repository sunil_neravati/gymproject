package com.epam.reporting.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.epam.reporting.dao.ReportingRepository;
import com.epam.reporting.dto.TrainerStatusDTO;
import com.epam.reporting.dto.TrainingReportDTO;
import com.epam.reporting.dto.TrainingSummaryResponse;
import com.epam.reporting.entity.TrainerStatus;
import com.epam.reporting.entity.TrainingSummary;
import com.epam.reporting.exception.ReportException;
import com.epam.reporting.service.ReportingService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReportingServiceImpl implements ReportingService {

	private final ReportingRepository reportingRepository;

	@Override
	@KafkaListener(topics = "report-2", groupId = "myGroup1")
	public void updateReport(TrainingReportDTO trainingReportDTO) {

		log.info("Entered updateReport method for trainer", trainingReportDTO.getUsername());
		TrainingSummary trainingSummary = reportingRepository.findById(trainingReportDTO.getUsername())
				.orElseGet(()->TrainingSummary.builder().firstName(trainingReportDTO.getFirstName())
						.lastName(trainingReportDTO.getLastName()).username(trainingReportDTO.getUsername())
						.status(Boolean.TRUE.equals(trainingReportDTO.getIsActive()) ? TrainerStatus.ACTIVE
								: TrainerStatus.INACTIVE)
						.duration(new HashMap<>()).build());

		Map<Integer, Map<Integer, Map<Integer, Integer>>> result = trainingSummary.getDuration();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(trainingReportDTO.getTrainingDate());

		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		Map<Integer, Map<Integer, Integer>> yearMap = result.getOrDefault(year, new HashMap<>());
		Map<Integer, Integer> monthMap = yearMap.getOrDefault(month, new HashMap<>());
		monthMap.put(day, monthMap.getOrDefault(day, 0) + trainingReportDTO.getTrainingDuration());
		yearMap.put(month, monthMap);
		result.put(year, yearMap);

		reportingRepository.save(trainingSummary);

	}

	@Override
	public TrainingSummaryResponse getReport(String username) {
		log.info("Entered getReport method for traier", username);
		TrainingSummary trainingSummary = reportingRepository.findById(username)
				.orElseThrow(() -> new ReportException("user report not found"));
		return getTrainingSummaryResponse(username, trainingSummary);
	}

	private TrainingSummaryResponse getTrainingSummaryResponse(String username, TrainingSummary trainingSummary) {
		TrainingSummaryResponse trainingSummaryResponse = new TrainingSummaryResponse();
		trainingSummaryResponse.setDuration(trainingSummary.getDuration());
		trainingSummaryResponse.setFirstName(trainingSummary.getFirstName());
		trainingSummaryResponse.setLastName(trainingSummary.getLastName());
		trainingSummaryResponse.setStatus(trainingSummary.getStatus() == TrainerStatus.ACTIVE ? TrainerStatusDTO.ACTIVE
				: TrainerStatusDTO.INACTIVE);
		trainingSummaryResponse.setUsername(username);
		return trainingSummaryResponse;
	}

}
