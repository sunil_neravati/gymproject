package com.epam.reporting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.reporting.dao.ReportingRepository;
import com.epam.reporting.dto.TrainingReportDTO;
import com.epam.reporting.dto.TrainingSummaryResponse;
import com.epam.reporting.entity.TrainerStatus;
import com.epam.reporting.entity.TrainingSummary;
import com.epam.reporting.exception.ReportException;
import com.epam.reporting.service.impl.ReportingServiceImpl;

@ExtendWith(MockitoExtension.class)
class ReportingServiceApplicationTests {

	@Mock
	private ReportingRepository reportingRepository;

	@InjectMocks
	private ReportingServiceImpl reportingService;

	TrainingReportDTO trainingReportDTO;
	TrainingReportDTO trainingReportDTO1;
	TrainingSummary existingSummary;

	@BeforeEach
	void setup() {
		trainingReportDTO = new TrainingReportDTO();
		trainingReportDTO.setUsername("username");
		trainingReportDTO.setFirstName("John");
		trainingReportDTO.setLastName("Doe");
		trainingReportDTO.setIsActive(true);
		trainingReportDTO.setTrainingDate(new Date());
		trainingReportDTO.setTrainingDuration(60);

		trainingReportDTO1 = new TrainingReportDTO();
		trainingReportDTO1.setUsername("username");
		trainingReportDTO1.setFirstName("John");
		trainingReportDTO1.setLastName("Doe");
		trainingReportDTO1.setIsActive(false);
		trainingReportDTO1.setTrainingDate(new Date());
		trainingReportDTO1.setTrainingDuration(60);

		existingSummary = new TrainingSummary();
		existingSummary.setUsername("username");
		existingSummary.setStatus(TrainerStatus.ACTIVE);
		existingSummary.setDuration(new HashMap<>());
	}

	@Test
	void testUpdateReport() {
		when(reportingRepository.findById("username")).thenReturn(Optional.of(existingSummary));
		when(reportingRepository.save(any())).thenReturn(null);
		reportingService.updateReport(trainingReportDTO);
		verify(reportingRepository, times(1)).findById("username");
		verify(reportingRepository, times(1)).save(any());
	}

	@Test
	void testGetReport() {
		existingSummary.setStatus(TrainerStatus.INACTIVE);
		when(reportingRepository.findById("username")).thenReturn(Optional.of(existingSummary));
		TrainingSummaryResponse result = reportingService.getReport("username");
		verify(reportingRepository, times(1)).findById("username");
		assertEquals("username", result.getUsername());
		assertEquals(existingSummary.getDuration(), result.getDuration());
	}
	
	@Test
	void testGetReportWithInactiveUser() {
		
		when(reportingRepository.findById("username")).thenReturn(Optional.of(existingSummary));
		TrainingSummaryResponse result = reportingService.getReport("username");
		verify(reportingRepository, times(1)).findById("username");
		assertEquals("username", result.getUsername());
		assertEquals(existingSummary.getDuration(), result.getDuration());
	}

	@Test
	void testGetReportWithInvalidUser() {
		when(reportingRepository.findById("username")).thenReturn(Optional.empty());
	   assertThrows(ReportException.class, ()-> reportingService.getReport("username"));
	
	}

	@Test
	void testUpdateReportUserNotFound() {
	    when(reportingRepository.findById("username")).thenReturn(Optional.empty());
	    reportingService.updateReport(trainingReportDTO);
	    verify(reportingRepository, times(1)).findById("username");
	    verify(reportingRepository, times(1)).save(any());
	}

	@Test
	void testUpdateReportUserNotFoundWithInactiveStatus() {
	    when(reportingRepository.findById("username")).thenReturn(Optional.empty());
	    reportingService.updateReport(trainingReportDTO1);
	    verify(reportingRepository, times(1)).findById("username");
	    verify(reportingRepository, times(1)).save(any());
	}

}
